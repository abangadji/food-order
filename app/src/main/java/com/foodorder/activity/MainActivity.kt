package com.foodorder.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.foodorder.R
import com.foodorder.adapter.OrdersAdapter
import com.foodorder.model.Order
import com.foodorder.utils.getOrdersList
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val updatedOrderList = mutableListOf<Order>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getOrdersList().forEach { order ->
            if (contains(order.date)) {
                updatedOrderList.add(order)
            } else {
                updatedOrderList.add(
                    Order(
                        order.orderId,
                        order.name,
                        order.orderDate,
                        true
                    )
                )
                updatedOrderList.add(order)
            }
        }
        rvOrders.adapter = OrdersAdapter(updatedOrderList)
    }

    private fun contains(date: String?): Boolean {
        for (order in updatedOrderList) {
            if (order.date == date) {
                return true
            }
        }
        return false
    }
}