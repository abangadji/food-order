package com.foodorder.utils

import com.foodorder.model.Order

fun getOrdersList(): MutableList<Order> {
    val orderList = mutableListOf<Order>()
    orderList.add(Order(1, "Biryani", "09-04-2020 09:30 PM", false))
    orderList.add(Order(2, "Dosa", "09-04-2020 10:00 PM", false))
    orderList.add(Order(3, "Idly", "10-04-2020 08:30 AM", false))
    orderList.add(Order(4, "Dosa", "10-04-2020 09:30 AM", false))
    orderList.add(Order(5, "Biryani", "11-04-2020 10:30 PM", false))
    orderList.add(Order(6, "Dosa", "11-04-2020 10:00 PM", false))
    orderList.add(Order(7, "Biryani", "12-04-2020 09:30 PM", false))
    orderList.add(Order(8, "Idly", "12-04-2020 08:30 AM", false))
    orderList.add(Order(9, "Dosa", "13-04-2020 09:30 AM", false))
    orderList.add(Order(10, "Biryani", "13-04-2020 10:30 PM", false))
    return orderList
}